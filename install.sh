#!/bin/bash

# Find the windows username
WUSER=$(cmd.exe /c "echo %USERNAME%" 2> /dev/null | sed -e 's/\r//g')
WUSER=${WUSER:-$USER}

# Install the packages
sudo dpkg -i linux-*.deb

# Determine the filename of the installed kernel file
VERSION=$(ls linux-image* | grep -Eo -m1 'linux-image-[0-9\.]+')
KERNEL=$(cd /boot/;ls ${VERSION/linux-image/vmlinuz}*)

# Copy the kernel to the user home directory
cp /boot/${KERNEL} /mnt/c/Users/$WUSER/.${KERNEL}

# Configure WSL to use this copied kernel
WSLCONFIG=/mnt/c/Users/$WUSER/.wslconfig
if [ ! -e ${WSLCONFIG} ]; then 
	echo "[wsl2]" > ${WSLCONFIG}; 
fi
chmod +w ${WSLCONFIG}

# Commend out any previous kernel settings
sed -i 's:^kernel=:#kernel=:g' ${WSLCONFIG}

# Add the new kernel setting
echo 'kernel=C:\\Users\\'"$WUSER"'\\.'"${KERNEL}" >> ${WSLCONFIG}

echo "Finished, now restart WSL"
